var comparisonStatus = require('./comparisonStatus');

function RowComparison(index, status, left, right) {
    if (!(this instanceof RowComparison)) {
        throw new Error('RowComparison is a class. Use constuctor: new RowComparison(...)');
    }

    this.index = index;
    this.status = status;
    this.left = left;
    this.right = right;
}

var _map = {};
_map[comparisonStatus.Equal.toString()] = " ";
_map[comparisonStatus.Similar.toString()] = "*";
_map[comparisonStatus.Left.toString()] = "-";
_map[comparisonStatus.Right.toString()] = "+";

// console.log('Map: ' + JSON.stringify(_map));

RowComparison.prototype.getSymbol = function() {
    // console.log('Status: ' + this.status + '; Symbol: ' + _map[this.status.toString()]);

    return _map[this.status.toString()];
};

RowComparison.prototype.getPrintableValue = function() {
    switch (this.status) {
        case comparisonStatus.Equal: return this.left;
        case comparisonStatus.Similar: return this.left + '|' + this.right;
        case comparisonStatus.Left: return this.left;
        case comparisonStatus.Right: return this.right;
        default: throw new Error('Unknown comparison status');
    }
}

RowComparison.prototype.toString = function () {
    return /*"  " + this.index +*/ "\t" + this.getSymbol() + "\t" + this.getPrintableValue();
}

// static "helpers":

RowComparison.equal = function(index, text) {
    return new RowComparison(index, comparisonStatus.Equal, text, text);
};

RowComparison.left = function(index, text) {
    if (text === null) throw new Error("Left text cannot be null");
    return new RowComparison(index, comparisonStatus.Left, text, null);
};

RowComparison.right = function(index, text) {
    if (text === null) throw new Error("Right text cannot be null");
    return new RowComparison(index, comparisonStatus.Right, null, text);
};

RowComparison.similar = function(index, left, right) {
    if (left === null || right === null) throw new Error("Similar text cannot be null");
    return new RowComparison(index, comparisonStatus.Similar, left, right);
};

module.exports = RowComparison;
