// Comapres two sections which have only one equal line between each other

var RowComparison = require('./rowComparison');
var comparisonStatus = require('./comparisonStatus');
var stringComparer = require('./stringComparer');

function Section(section1, section2) {
    if (!(this instanceof Section)) {
        throw new Error('Section is a class. Use constuctor: new Section(...)');
    }

    this.section1 = section1;
    this.section2 = section2;

    _checkInputs(section1, section2);
    _addMissedRows(section1, section2);

    this.comparison = _compare(section1, section2);
}

function _checkInputs(section1, section2) {
    var equalLinesCount = 0;
    var i, j;
    for (i = 0; i < section1.length; i++) {
        for (j = 0; j < section2.length; j++) {
            if (section1[i] === section2[j]) {
                equalLinesCount++;
                break; // itterate only to first occurence
            }
        }
    }

    if (equalLinesCount > 1) {
        throw new Error('Sections contains more than one equal lines pair');
    }
}

// make section eqaul by length - by edding nulls
function _addMissedRows(section1, section2) {
    while (section1.length < section2.length) {
        section1.push(null);
    }

    while (section1.length > section2.length) {
        section2.push(null);
    }
}

function _compare(section1, section2) {
    if (section1.length !== section2.length) {
        throw new Error('Sections must have equal length');
    }

    var sectionComparison = [];
    var left, right;

    var i = 0, shift = 0;
    var length = section1.length;
    while (i < length) {
        left = section1[i];
        right = section2[i];

        if (stringComparer.isEqual(left, right)) {
            sectionComparison.push(RowComparison.equal(i + shift, left));
        } else if (stringComparer.isLeft(left, right)) {
            sectionComparison.push(RowComparison.left(i + shift, left));
        } else if (stringComparer.isRight(left, right)) {
            sectionComparison.push(RowComparison.right(i + shift, right));
        } else if (stringComparer.isSimilar(left, right)) {
            sectionComparison.push(RowComparison.similar(i + shift, left, right));
        } else {
            sectionComparison.push(RowComparison.left(i + shift, left));
            shift++; // each not equal row increases result length by 1
            sectionComparison.push(RowComparison.right(i + shift, right));
        }

        i++;
    }

    return sectionComparison;
}

module.exports = Section;
