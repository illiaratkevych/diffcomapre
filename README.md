##DiffComapre

Simple text files comparison tool.

##How to run
Just type in the console:

`node index file1.txt file2.txt`

or

`npm test`

##Note
You can clone this repo or download zip archive from the source tab.
