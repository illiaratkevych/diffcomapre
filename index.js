var fs = require('fs');
var comparer = require('./comparer');

var args = process.argv.slice(2);
if (args.length !== 2) {
    _logError('Provide two arguments - existed text files');
    process.exit(-1);
}

_checkFileExists(args[0]);
_checkFileExists(args[1]);

var file1 = fs.readFileSync(args[0], 'utf8');
var file2 = fs.readFileSync(args[1], 'utf8');

var comparisonResult;
try {
    comparisonResult = comparer.compare(file1, file2);
    _printResult(comparisonResult);
} catch (e) {
    _logError(e, e.stack);
}

function _checkFileExists(path) {
    try {
        fs.accessSync(path, fs.F_OK);
        console.log('File [' + path + '] status OK');
    } catch (e) {
        _logError('File [' + path + '] does not exists');
    }
}

function _printResult(comparison) {
    for (var i = 1; i < comparison.length; i++) { // skip first line since it was added programatically
        console.log('  ' + i + comparison[i].toString());
    }
}

function _logError(message, stack) {
    console.log('ERROR: ' + message);
    if (stack) {
        console.log(stack);
    }

    process.exit(-1);
}
