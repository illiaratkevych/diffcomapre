var _statuses = {
    Equal: 0,
    Similar: 1,
    Left: 2,
    Right: 3
};

module.exports = _statuses;
