var Section = require('./section');

function _compare(file1, file2) {
    var lines1 = file1.split('\n');
    var lines2 = file2.split('\n');

    lines1.splice(0, 0, null);
    lines2.splice(0, 0, null);

    var equal = _getEqualLines(lines1, lines2);

    var rawSections = _splitOnSections(lines1, lines2, equal);
    var comparisonResult = [];
    for (var i = 0; i < rawSections.length; i++) {
        var processedSection = new Section(rawSections[i].left, rawSections[i].right);
        comparisonResult = comparisonResult.concat(processedSection.comparison);
    }

    return comparisonResult;
}

function _getEqualLines(lines1, lines2) {
    var equal = [];

    // get equal lines:
    for (var i = 0; i < lines1.length; i++) {
        for (var j = 0; j < lines2.length; j++) {
            if (lines1[i] === lines2[j]) {
                equal.push({ text: lines1[i], leftIndex: i, rightIndex: j });
                break; // important! to skip repeated occurences
            }
        }
    }

    return equal;
}

function _splitOnSections(lines1, lines2, equal) {
    var sections = [];
    var leftPositions = equal.map(function(el) { return el.leftIndex; });
    var rightPositions = equal.map(function(el) { return el.rightIndex; });
    var leftSections = _splitArrayOnSections(lines1, leftPositions);
    var rightSections = _splitArrayOnSections(lines2, rightPositions);
    for (var i = 0; i < equal.length; i++) {
        sections.push({ left: leftSections[i], right: rightSections[i] });
    }

    return sections;
}

// splits array [1, 3, 5, -2, 6, -11] by positions [0, 3, 4] into sections [[1, 3, 5], [-2], [6, -11]]
function _splitArrayOnSections(array, positions) {
    var i = 0,
        sections = [],
        section;
    while (i < positions.length - 1) {
        section = array.slice(positions[i], positions[i+1]);
        if (section.length > 0) sections.push(section);
        i++;
    }

    // add last section:
    if (positions[positions.length - 1] !== positions.length) {
        // start at last index in positions, end - end of the array
        section = array.slice(positions[positions.length - 1]);
        if (section.length > 0) sections.push(section);
    }

    return sections;
}

module.exports = {
    compare: _compare
};
