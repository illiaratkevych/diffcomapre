function _isEqual(left, right) {
    return left === right || (left === null && right === null);
}

function _isLeft(left, right) {
    return left !== null && right === null;
}

function _isRight(left, right) {
    return left === null && right !== null;
}

// this method should be specified clearly in requirements. Here I assume that lines are similar
// if thay have at least two equal symbol in common
var _similarCriteria = 2;
function _isSimilar(left, right) {
    if (left === null || right === null || _isEqual(left, right)) return false;

    var i, j, equalSymbolsCount = 0;
    for (i = 0; i < left.length; i++) {
        for (j = 0; j < right.length; j++) {
            if (left[i] === right[j]) equalSymbolsCount++;
        }
    }

    return equalSymbolsCount >= _similarCriteria;
}

module.exports = {
    isEqual: _isEqual,
    isLeft: _isLeft,
    isRight: _isRight,
    isSimilar: _isSimilar
};
